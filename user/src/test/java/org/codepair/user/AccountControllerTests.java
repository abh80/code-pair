package org.codepair.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.codepair.user.controllers.AccountManagement;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(AccountManagement.class)
public class AccountControllerTests {
    @Autowired
    private MockMvc mockMvc;
}