package org.codepair.user.requestBody;


public record SignupReqBody(String email, String first_name, String last_name, String username, String password) {
}
