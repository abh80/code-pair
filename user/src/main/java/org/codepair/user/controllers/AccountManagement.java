package org.codepair.user.controllers;


import org.codepair.common.Constants.ErrorTrace;
import org.codepair.common.ErrorResponse;
import org.codepair.common.user.SnowflakeTool;
import org.codepair.user.abstractModels.AbstractAccount;
import org.codepair.user.abstractModels.LoginAbstractAccount;
import org.codepair.user.model.Account;
import org.codepair.user.repository.AccountRepository;
import org.codepair.user.requestBody.LoginBody;
import org.codepair.user.requestBody.SignupReqBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/api/v1"})
public class AccountManagement {
    private final AccountRepository accountRepository;

    @Autowired
    public AccountManagement(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @PostMapping("/external/signup")
    ResponseEntity<?> signup(@RequestBody SignupReqBody body) {
        if (body.email() == null || body.password() == null || body.username() == null || body.first_name() == null)
            return new ResponseEntity<>(new ErrorResponse("`email`, `password`, `username`, `first_name` are required parameters", HttpStatus.BAD_REQUEST.value(), ErrorTrace.INSUFFICIENT_DETAILS_PROVIDED), HttpStatus.BAD_REQUEST);

        boolean email_exists = accountRepository.existsAccountByEmail(body.email());
        if (email_exists) {
            return new ResponseEntity<>(new ErrorResponse("Email already exists", HttpStatus.CONFLICT.value(), ErrorTrace.EMAIL_EXISTS), HttpStatus.CONFLICT);
        }
        if (body.password().length() < 8) {
            return new ResponseEntity<>(new ErrorResponse("Password must be at least 8 characters long", HttpStatus.BAD_REQUEST.value(), ErrorTrace.PASSWORD_TOO_SHORT), HttpStatus.BAD_REQUEST);
        }
        Account account = Account.builder().email(body.email()).username(body.username()).last_name(body.last_name()).first_name(body.first_name()).uuid(SnowflakeTool.generateUUID()).build();
        account.hashAndSetPassword(body.password());

        account = accountRepository.save(account);

        return new ResponseEntity<>(AbstractAccount.handle(account), HttpStatus.OK);
    }

    @PostMapping("/external/login")
    ResponseEntity<?> login(@RequestBody LoginBody body) {
        if (body.email() == null && body.username() == null)
            return new ResponseEntity<>(new ErrorResponse("Both username and email fields are blank, expected one of them to be filled", HttpStatus.BAD_REQUEST.value(), ErrorTrace.INSUFFICIENT_DETAILS_PROVIDED), HttpStatus.BAD_REQUEST);
        String password = body.email() != null ? accountRepository.getPasswordByEmail(body.email()) : accountRepository.getPasswordByUsername(body.username());
        if (password == null) {
            return new ResponseEntity<>(new ErrorResponse("`username` or `password` may be incorrect, please try again", HttpStatus.UNAUTHORIZED.value(), ErrorTrace.INCORRECT_CREDENTIALS), HttpStatus.UNAUTHORIZED);
        } else if (!bCryptPasswordEncoder.matches(body.password(), password)) {
            return new ResponseEntity<>(new ErrorResponse("`username` or `password` may be incorrect, please try again", HttpStatus.UNAUTHORIZED.value(), ErrorTrace.INCORRECT_CREDENTIALS), HttpStatus.UNAUTHORIZED);
        }
        Account account = body.email() != null ? accountRepository.findAccountByEmail(body.email()) : accountRepository.findAccountByUsername(body.username());
        if (account == null)
            return new ResponseEntity<>(new ErrorResponse("`username` or `password` may be incorrect, please try again", HttpStatus.UNAUTHORIZED.value(), ErrorTrace.INCORRECT_CREDENTIALS), HttpStatus.UNAUTHORIZED);

        if (account.getToken() == null){
            account.setToken(SnowflakeTool.generateToken(account.getUuid()));
            account = accountRepository.save(account);
        }
        return new ResponseEntity<>(LoginAbstractAccount.handle(account),  HttpStatus.OK);
    }
}
