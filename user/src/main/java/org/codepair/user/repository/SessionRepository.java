package org.codepair.user.repository;

import org.codepair.user.model.Session;
import org.springframework.data.repository.CrudRepository;

public interface SessionRepository extends CrudRepository<Session , Long> {
}
