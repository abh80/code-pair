package org.codepair.user.repository;

import org.codepair.user.model.Account;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    boolean existsAccountByEmail(@Param("email") String email);

    Account findAccountByEmail(@Param("email") String email);

    Account findAccountByUsername(@Param("username") String username);

    @Query(value = "SELECT password from \"user_db\".\"ACCOUNT\" where email = :email")
    String getPasswordByEmail(@Param("email") String email);

    @Query(value = "SELECT password from \"user_db\".\"ACCOUNT\" where username = :username")
    String getPasswordByUsername(@Param("username") String username);
}
