package org.codepair.user.abstractModels;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AbstractAccount {
    public long id;
    public String first_name;
    public String last_name;
    public String email;
    public String username;
    public long uuid;

    public static AbstractAccount handle(org.codepair.user.model.Account account) {
        AbstractAccount acc = AbstractAccount.builder().build();
        acc.first_name = account.getFirst_name();
        acc.id = account.getId();
        acc.last_name = account.getLast_name();
        acc.email = account.getEmail();
        acc.username = account.getUsername();
        acc.uuid = account.getUuid();
        return acc;
    }
}
