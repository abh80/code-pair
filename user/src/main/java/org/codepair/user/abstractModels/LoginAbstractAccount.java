package org.codepair.user.abstractModels;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class LoginAbstractAccount {
    public long id;
    public String first_name;
    public String last_name;
    public String email;
    public String username;
    public String token;
    public long uuid;

    public static LoginAbstractAccount handle(org.codepair.user.model.Account account) {
        LoginAbstractAccount acc = LoginAbstractAccount.builder().build();
        acc.first_name = account.getFirst_name();
        acc.id = account.getId();
        acc.last_name = account.getLast_name();
        acc.email = account.getEmail();
        acc.username = account.getUsername();
        acc.token = account.getToken();
        acc.uuid = account.getUuid();
        return acc;
    }
}
