package org.codepair.user;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@SpringBootApplication(scanBasePackages = {"org.codepair.user"})
@EnableCassandraRepositories("org.codepair.user.repository")
public class Main {
    public static void main(String... args) {
        SpringApplication.run(Main.class);
    }
}
