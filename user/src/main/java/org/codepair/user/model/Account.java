package org.codepair.user.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.*;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table("ACCOUNT")
public class Account {
    @PrimaryKeyColumn(name = "id", type = PrimaryKeyType.PARTITIONED)
    private long id;

    @Indexed
    private long uuid;

    private String first_name;
    private String last_name;
    @Indexed
    private String email;
    @Indexed
    private String username;
    private String password;
    private String token;

    public String hashAndSetPassword(String password) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        this.password = bCryptPasswordEncoder.encode(password);
        return this.password;
    }
    public static void main(String[] args){
        System.out.println(Account.builder().build().hashAndSetPassword("abhinav277"));
    }
}
