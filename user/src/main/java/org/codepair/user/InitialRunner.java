package org.codepair.user;

import org.codepair.common.user.SnowflakeTool;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class InitialRunner {
    @EventListener(ApplicationReadyEvent.class)
    public void run() {
        long pid = ProcessHandle.current().pid();
        SnowflakeTool.setWorkerId(pid);
        Logger.getLogger(InitialRunner.class.getName()).log(Level.INFO, "WORKER ID set to {0}", new Object[]{pid});

    }

}
