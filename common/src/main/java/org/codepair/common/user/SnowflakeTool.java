package org.codepair.common.user;

import java.nio.Buffer;
import java.util.Base64;

public class SnowflakeTool {
    // Variables
    public static int counter = 0;
    private static long last_timestamp = -1L;
    private static long sequence = 0L;
    private static long workerId;


    // Constants
    private static final int MAX_COUNTER = 15;
    private static final long BASE_EPOCH = 1589995002000L;
    private static final int WORKER_ID_BITS = 10;
    private static final int SEQUENCE_BITS = 12;
    private static final int LENGTH_TOKEN_RANDOM_STRING = 16;

    public static void setWorkerId(long w_id) {
        workerId = w_id;
    }

    public static synchronized String generateToken(long uuid) {
        String alpha_num = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder token = new StringBuilder();
        token.append(uuid);
        token.append("-");
        for (int i : new int[LENGTH_TOKEN_RANDOM_STRING]) {
            token.append(alpha_num.charAt((int) Math.floor(Math.random() * LENGTH_TOKEN_RANDOM_STRING)));
        }
        if (counter >= MAX_COUNTER) {
            counter = 0;
        }
        token.append("-");
        token.append(workerId);
        token.append("-");
        token.append(counter);
        counter++;
        return Base64.getEncoder().encodeToString(token.toString().getBytes());
    }

    public static synchronized long generateUUID() {
        long timestamp = System.currentTimeMillis();
        if (timestamp != last_timestamp) {
            last_timestamp = timestamp;
            sequence = 0L;
        } else {
            sequence = (sequence + 1) % (1 << SEQUENCE_BITS);
            if (sequence == 0) {
                while (timestamp <= last_timestamp) {
                    timestamp = System.currentTimeMillis() - BASE_EPOCH;
                }
                last_timestamp = timestamp;
            }
        }

        return (timestamp << (WORKER_ID_BITS + SEQUENCE_BITS)) |
                (workerId << SEQUENCE_BITS) |
                sequence;

    }
}
