package org.codepair.common.Constants;

public enum ErrorTrace {
    EMAIL_EXISTS,
    INSUFFICIENT_DETAILS_PROVIDED,
    PASSWORD_TOO_SHORT,
    INCORRECT_CREDENTIALS
}
