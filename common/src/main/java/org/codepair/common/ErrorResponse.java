package org.codepair.common;


import org.codepair.common.Constants.ErrorTrace;

public record ErrorResponse(String message, int status_code, ErrorTrace trace) {

}
